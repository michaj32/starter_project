from celery import shared_task
from django.core.mail import send_mail
from django.contrib.auth.models import User


@shared_task()
def sendmail(email_adress):
    send_mail(
        'Registration',
        'Registration has been succesful',
        'jmichaj.mail@gmail.com',
        [email_adress],
        fail_silently=False,
    )