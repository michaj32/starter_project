from django.urls import path, re_path
from . import serializers
from .views import login
from .views import CreateUserView


urlpatterns = [
    path('register/', CreateUserView.as_view()),
    path('login/', login),
    ]