from django.test import TestCase, Client
from .views import CreateUserView
from django.contrib.auth.models import User


class UserTestLogin(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser1', password='12345')

    def test_user_login(self):
        c = Client()
        response = c.post('/login/', {'username': 'testuser1', 'password': '12345'})
        self.assertEqual(response.status_code, 200)

    def tearDown(self):
        self.user.delete()


class UserTestReg(TestCase):

    def test_user_registration(self):
        c = Client()
        response = c.post('/register/', {'username': 'testuser1', 'password': '12345', 'email': 'test@local.host'})
        self.assertEqual(response.status_code, 201)