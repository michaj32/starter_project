from .models import Note
from rest_framework import generics
from .serializers import NoteSerializer, NoteSaveSerializer
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_201_CREATED, HTTP_200_OK
from rest_framework.response import Response
from rest_framework.authtoken.models import Token


class FilterNotes(generics.ListCreateAPIView):
    serializer_class = NoteSerializer

    def get_queryset(self):
        queryset = Note.objects.all()
        username = self.request.query_params.get('username', None)
        id = self.request.query_params.get('id', None)
        if not (username or id):
            return Note.objects.none()
        if username:
            queryset = queryset.filter(author__username=username)
        if id:
            queryset = queryset.filter(id=id)
        return queryset, Response(status=HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        token = request.data.get('token', None)
        if not token:
            return Response(status=HTTP_400_BAD_REQUEST)
        title = request.data.get('title')
        content = request.data.get('content')
        token = Token.objects.filter(key=token)
        if not token:
            return Response(status=HTTP_403_FORBIDDEN)
        token = token.first()
        user = token.user
        data = {
            "author": user.pk,
            "title": title,
            "content": content,
        }
        serializer = NoteSaveSerializer(data=data)
        if not serializer.is_valid():
            return Response(status=HTTP_400_BAD_REQUEST)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=HTTP_201_CREATED, headers=headers)
