from rest_framework import serializers
from . import models


class NoteSerializer(serializers.ModelSerializer):
    author = serializers.CharField(read_only=True, source='author.username')

    class Meta:
        fields = ['id', 'author', 'title', 'content']
        model = models.Note


class NoteSaveSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ['author', 'title', 'content']
        model = models.Note
