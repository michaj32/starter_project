from django.test import TestCase, RequestFactory, Client
from django.urls import reverse
from rest_framework import response
from .models import Note
from .views import FilterNotes
from django.contrib.auth.models import User
from rest_framework.test import force_authenticate
from rest_framework.authtoken.models import Token


class NoteTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser1', password='12345')

    def create_note(self):
        return Note.objects.create(author=self.user,
                                   title='test',
                                   content='test',
                                   id=1)

    def test_note(self):
        a = self.create_note()
        self.assertTrue(isinstance(a, Note))
        self.assertEqual(a.author, self.user)
        self.assertEqual(a.__str__(), a.title)

    def tearDown(self):
        self.user.delete()


class NoteViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser1', password='12345')
        self.factory = RequestFactory()

    def create_note(self):
        return Note.objects.create(author=self.user,
                                   title='test',
                                   content='test')

    def test_note_post(self):
        """
        Create new note with all credentials
        """
        c = Client()
        token = Token.objects.get_or_create(user=self.user)[0]
        request = c.post('/notes/', {'title': 'test', 'content': 'test', 'token': token})
        self.assertEqual(request.status_code, 201)

    def test_note_post_no_token(self):
        """
        Trying to create new note without token
        """
        c = Client()
        request = c.post('/notes/', {'title': 'test', 'content': 'test'})
        self.assertEqual(request.status_code, 400)

    def test_note_author_view(self):

        a = self.create_note()
        request = self.factory.get('notes/testuser1')
        request.user = self.user
        response = FilterNotes.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def tearDown(self):
        self.user.delete()
