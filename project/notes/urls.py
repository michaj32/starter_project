from django.urls import path
from .views import FilterNotes
from rest_framework.routers import DefaultRouter


urlpatterns = [
    path('', FilterNotes.as_view(), name='notes')
]

