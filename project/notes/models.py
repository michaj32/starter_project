from django.db import models
from django.conf import settings
from django.contrib.auth.models import User


class Note(models.Model):
    author = models.ForeignKey(User, related_name='author', on_delete=models.CASCADE)
    title = models.CharField(max_length=150)
    content = models.TextField()

    def __str__(self):
        return self.title
